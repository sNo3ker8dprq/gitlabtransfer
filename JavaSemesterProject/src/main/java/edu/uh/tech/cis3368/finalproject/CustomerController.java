package edu.uh.tech.cis3368.finalproject;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class CustomerController extends CustomerService{

    @FXML
    public TextField t1;
    public TextField t2;
    public TextField t3;
    public GridPane gridPane;

    DesignEntity d;
    CustomerService c;
    PartEntity pp = new PartEntity();


    public void submitButton(ActionEvent actionEvent) {
        if(t1.getText().equals("") || t1.getText().equals("")
                || t1.getText().equals("")){
            System.out.println("Missing field"); }
                else {
                    try {
                        d = new DesignEntity();
                        d.setDesignName(t3.getText());

                        c = new CustomerService();
                        c.setLastName(t2.getText());
                        c.setFirstName(t1.getText());

                        save(c);
                        d.save(d);
                    }catch(Exception e){e.printStackTrace();}
        }
    }

    public void homeButton(ActionEvent actionEvent) {
        Parent rt = null;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("main.fxml"));
            rt = fxmlLoader.load();
        }catch(Exception e){System.out.println("There's a possible error.");}
        Stage belayer = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
        belayer.setScene(new Scene(rt,600,400));
    }

    public void addParts() {

        Set<Node> deleteNodes = new HashSet<>();
        for (Node child : gridPane.getChildren()) {

            Integer rowIndex = GridPane.getRowIndex(child);

            int r = rowIndex == null ? 0 : rowIndex;

            if (r > gridPane.getRowCount()) {

                GridPane.setRowIndex(child, r-1);
            } else if (r == gridPane.getRowCount()) {

                deleteNodes.add(child);
            }
        }
        // Removes all nodes in the table
        gridPane.getChildren().removeAll(deleteNodes);

        int i;
        PartEntity p = null;
        Label la = null;
        for(i=1; i<=15; i++) {
            p = pp.partAdder(i);


            la = new Label(p.getComponentName() + " & $" + p.getCost());
            gridPane.addRow(i-1, la);
        }

    }
}
