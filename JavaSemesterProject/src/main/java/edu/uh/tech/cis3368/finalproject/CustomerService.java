package edu.uh.tech.cis3368.finalproject;

import org.hibernate.service.spi.InjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.util.BeanDefinitionUtils;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.Objects;
import java.util.Optional;


@Service
@Table(name = "customer", schema = "public", catalog = "finalproject")
public class CustomerService implements customerRepository{
    private Integer customerId;
    private String lastName;
    private String firstName;
    private DesignEntity design;


    public CustomerService(String lastName, String firstName){
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public CustomerService(Integer customerId, String lastName, String firstName){
        this.customerId = customerId;
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public CustomerService() {
    }

    //@Autowired
    //@SuppressWarnings("SpringJavaAutowiringInspection")
    //public void setRepository(customerRepository r){
    //    cres = r;
    //}

    @Id
    @Column(name = "customer_id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Nullable
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Basic
    @Column(name = "last_name", nullable = false)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "first_name", nullable = false)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomerService that = (CustomerService) o;
        return customerId == that.customerId &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(firstName, that.firstName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerId, lastName, firstName);
    }

    @SuppressWarnings("JpaAttributeTypeInspection")
    @OneToMany(mappedBy = "customer")
    public DesignEntity getDesign() {
        return design;
    }

    public void setDesign(DesignEntity des) {
        design = des;
    }

    @Override
    public <S extends CustomerService> S save(S s) {
        return null;
    }

    @Override
    public <S extends CustomerService> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<CustomerService> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public Iterable<CustomerService> findAll() {
        return null;
    }

    @Override
    public Iterable<CustomerService> findAllById(Iterable<Integer> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(CustomerService customerService) {

    }

    @Override
    public void deleteAll(Iterable<? extends CustomerService> iterable) {

    }

    @Override
    public void deleteAll() {

    }
}
