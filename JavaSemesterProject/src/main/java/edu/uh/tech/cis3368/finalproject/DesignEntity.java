package edu.uh.tech.cis3368.finalproject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.Objects;
import java.util.Optional;

@Service
@Table(name = "design", schema = "public", catalog = "finalproject")
public class DesignEntity implements designRepository{
    private Integer designId;
    private String designName;
    private CustomerService customer;


    public DesignEntity(String d){
        designName = d;
    }

    public DesignEntity() {
    }


    //public void setRepository(designRepository r){
    //    dres = r;
    //}

    @Id
    @Column(name = "design_id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getDesignId() {
        return designId;
    }

    public void setDesignId(int designId) {
        this.designId = designId;
    }

    @Basic
    @Column(name = "design_name", nullable = false)
    public String getDesignName() {
        return designName;
    }

    public void setDesignName(String designName) {
        this.designName = designName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DesignEntity that = (DesignEntity) o;
        return designId == that.designId &&
                Objects.equals(designName, that.designName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designId, designName);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", referencedColumnName = "customer_id")
    public CustomerService getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerService custy) {
        customer = custy;
    }

    @Override
    public <S extends DesignEntity> S save(S s) {
        return null;
    }

    @Override
    public <S extends DesignEntity> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<DesignEntity> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public Iterable<DesignEntity> findAll() {
        return null;
    }

    @Override
    public Iterable<DesignEntity> findAllById(Iterable<Integer> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(DesignEntity designEntity) {

    }

    @Override
    public void deleteAll(Iterable<? extends DesignEntity> iterable) {

    }

    @Override
    public void deleteAll() {

    }
}
