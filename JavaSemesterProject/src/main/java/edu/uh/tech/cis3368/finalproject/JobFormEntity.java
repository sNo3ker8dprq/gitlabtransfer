package edu.uh.tech.cis3368.finalproject;

import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;
import java.util.Optional;

@Service
@Table(name = "job_form", schema = "public", catalog = "finalproject")
public class JobFormEntity implements job_formRepository{
    private int jobId;
    private String jobName;
    private Date completionDate;

    @Id
    @Column(name = "job_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "job_name")
    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @Basic
    @Column(name = "completion_date")
    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public JobFormEntity partAdder(int i){

        JobFormEntity j = new JobFormEntity();
        if(existsById(i))
            j = findById(i).get();
        return j;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobFormEntity that = (JobFormEntity) o;
        return jobId == that.jobId &&
                Objects.equals(jobName, that.jobName) &&
                Objects.equals(completionDate, that.completionDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobId, jobName, completionDate);
    }

    @Override
    public <S extends JobFormEntity> S save(S s) {
        return null;
    }

    @Override
    public <S extends JobFormEntity> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<JobFormEntity> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public Iterable<JobFormEntity> findAll() {
        return null;
    }

    @Override
    public Iterable<JobFormEntity> findAllById(Iterable<Integer> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(JobFormEntity jobFormEntity) {

    }

    @Override
    public void deleteAll(Iterable<? extends JobFormEntity> iterable) {

    }

    @Override
    public void deleteAll() {

    }
}
