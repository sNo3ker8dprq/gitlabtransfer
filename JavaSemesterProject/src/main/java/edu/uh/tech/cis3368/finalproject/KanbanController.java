package edu.uh.tech.cis3368.finalproject;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import javax.persistence.Column;

public class KanbanController {

    public Label lbl;
    public GridPane gridPane1;
    public GridPane gridPane2;
    public GridPane gridPane3;

    private JobFormEntity jj = new JobFormEntity();

    // For putting new jobs into pre-production
    public void addJob(){

        int i = 0;
        JobFormEntity j = null;
        Label la = null;
        j = jj.partAdder(i);

        la = new Label(j.getJobName() + " & $" + j.getCompletionDate());
        gridPane1.addRow(i - 1, la);
    }


    public void homeButton(ActionEvent actionEvent) {
        Parent rt = null;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("main.fxml"));
            rt = fxmlLoader.load();
        }catch(Exception e){System.out.println("There's a possible error.");}
        Stage belayer = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
        belayer.setScene(new Scene(rt,600,400));
    }

    public void onDragDetected(MouseEvent mouseEvent) {
        System.out.println("detected drag.");
        Dragboard dragboard = lbl.startDragAndDrop(TransferMode.COPY_OR_MOVE);
        ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(lbl.getText());
        dragboard.setContent(clipboardContent);
        mouseEvent.consume();
    }

    public void onDragOver(DragEvent dragEvent) {
        System.out.println("drag over.");
        Dragboard dragboard = dragEvent.getDragboard();
        dragEvent.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        dragEvent.consume();

    }

    public void dragDropped(DragEvent dragEvent) {
        Dragboard dragboard = dragEvent.getDragboard();
        String s = dragboard.getString();
        Label l = new Label(s);
        if(dragEvent.getTarget().equals(gridPane2))
            gridPane2.addRow(gridPane2.getRowCount(),l);
        else if(dragEvent.getTarget().equals(gridPane3))
            gridPane3.addRow(gridPane3.getRowCount(),l);
    }
}
