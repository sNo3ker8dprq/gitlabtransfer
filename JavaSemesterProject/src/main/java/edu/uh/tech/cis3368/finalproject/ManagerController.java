package edu.uh.tech.cis3368.finalproject;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.HashSet;
import java.util.Set;

public class ManagerController {

    public GridPane gridPane1;
    public GridPane gridPane2;

    PartEntity pp = new PartEntity();
    
    public void submitButton(ActionEvent actionEvent) {
    }

    public void calcTotal(ActionEvent actionEvent) {
    }

    public void kanbanLink(ActionEvent actionEvent) {
        Parent rt = null;
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Kanban.fxml"));
            rt = fxmlLoader.load();
        }catch(Exception e){System.out.println("There's a possible error.");}
        Stage belayer = (Stage) ((Node)actionEvent.getSource()).getScene().getWindow();
        belayer.setScene(new Scene(rt,800,600));
    }

    public void loadButton(ActionEvent actionEvent) {
        Set<Node> deleteNodes = new HashSet<>();
        for (Node child : gridPane1.getChildren()) {

            Integer rowIndex = GridPane.getRowIndex(child);

            int r = rowIndex == null ? 0 : rowIndex;

            if (r > gridPane1.getRowCount()) {

                GridPane.setRowIndex(child, r-1);
            } else if (r == gridPane1.getRowCount()) {

                deleteNodes.add(child);
            }
        }
        // Removes all nodes in the table
        gridPane1.getChildren().removeAll(deleteNodes);

        int i;
        PartEntity p = null;
        Label la = null;
        for(i=1; i<=15; i++) {
            p = pp.partAdder(i);


            la = new Label(p.getComponentName() + " & $" + p.getCost());
            gridPane1.addRow(i-1, la);
        }
    }

    public void loadDesign(ActionEvent actionEvent) {
    }

    public void loadPartThree(ActionEvent actionEvent) {
    }

    public void loadPartOne(ActionEvent actionEvent) {
    }

    public void loadPartTwo(ActionEvent actionEvent) {
    }
}
