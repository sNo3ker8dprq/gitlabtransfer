package edu.uh.tech.cis3368.finalproject;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Statement;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

@Service
@Table(name = "part", schema = "public", catalog = "finalproject")
public class PartEntity implements partRepository, Serializable{
    private int partId;
    private double cost;
    private String componentName;


    public PartEntity(){}

    @Id
    @Column(name = "part_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getPartId() {
        return partId;
    }

    public void setPartId(int partId) {
        this.partId = partId;
    }

    @Basic
    @Column(name = "cost")
    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    @Basic
    @Column(name = "component_name")
    @Nullable
    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public PartEntity partAdder(int i){

            PartEntity p = new PartEntity();
            Integer ii = i;
            if(findById(ii).isPresent())
                p = findAll().iterator().next();
            return p;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PartEntity that = (PartEntity) o;
        return partId == that.partId &&
                Double.compare(that.cost, cost) == 0 &&
                Objects.equals(componentName, that.componentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(partId, cost, componentName);
    }

    @Override
    public <S extends PartEntity> S save(S s) {
        return null;
    }

    @Override
    public <S extends PartEntity> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<PartEntity> findById(Integer integer) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Integer integer) {
        return false;
    }

    @Override
    public Iterable<PartEntity> findAll() {
        return null;
    }

    @Override
    public Iterable<PartEntity> findAllById(Iterable<Integer> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Integer integer) {

    }

    @Override
    public void delete(PartEntity partEntity) {

    }

    @Override
    public void deleteAll(Iterable<? extends PartEntity> iterable) {

    }

    @Override
    public void deleteAll() {

    }
}
