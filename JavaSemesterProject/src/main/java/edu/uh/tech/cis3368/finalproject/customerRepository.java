package edu.uh.tech.cis3368.finalproject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


//@ComponentScan("edu.uh.tech.cis3368.finalproject.customerRepository")
@Repository
public interface customerRepository extends CrudRepository<CustomerService,Integer> {
}