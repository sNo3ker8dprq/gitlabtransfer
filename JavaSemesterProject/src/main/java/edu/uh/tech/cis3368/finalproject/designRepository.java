package edu.uh.tech.cis3368.finalproject;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
//@ComponentScan("edu.uh.tech.cis3368.finalproject.designRepository")
public interface designRepository extends CrudRepository<DesignEntity,Integer> {
}
