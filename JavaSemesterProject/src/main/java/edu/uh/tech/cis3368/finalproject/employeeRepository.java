package edu.uh.tech.cis3368.finalproject;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface employeeRepository extends CrudRepository<EmployeeEntity,Integer> {
}
