package edu.uh.tech.cis3368.finalproject;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.stereotype.Repository;

@Repository
public interface job_formRepository extends CrudRepository<JobFormEntity, Integer> {
}
